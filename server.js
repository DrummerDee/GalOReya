const PORT = 9000;
require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const cors = require('cors')
const app = express()


app.use(cors())
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

// Connect to MongoDB
let db,
  dbConnectionStr = process.env.DB_STRING,
  dbName = 'gallery'

MongoClient.connect(dbConnectionStr, {
  useUnifiedTopology: true
})
  .then(client => {
    console.log(`Connected to ${dbName} Database`);
    db = client.db('gallery')
    const commentCollection = db.collection('comments');
    //tells express to use ejs as template 
    app.set('view engine', 'ejs')

    // grab Welcome page 
    app.get('/', (req, res) => {
      res.sendFile('index.html', { root: 'public' })

    })

    //grab the hall page 
    app.get('/Hall', (req, res) => {
      res.sendFile('Hall.html', { root: 'public' })

    })
    //grabs the art page 
    app.get('/Gallery', (req, res) => {
      res.sendFile('Gallery.html', { root: 'public' })

    })
    //grabs the bye page 
    app.get('/Bye', (req, res) => {
      res.sendFile('Bye.html', { root: 'public' })

    })  
    // grabs the comment room that needs ejs
    app.get('/Fourteen', (req, res) => {
      commentCollection.find().toArray()
        .then(results => {
          res.render('Fourteen.ejs', { comment: results })
        })
        .catch(error => console.log(error))
    })
    // use method post to get the comment page 
    app.post('/comment', (req, res) => {
      commentCollection.insertOne(req.body)
        .then(results => {
          console.log('Comment added')
          res.redirect('/Fourteen')
          console.log(results)
        })
        .catch(error => console.log(error))
    })
    // update comment 
    // app.put('/addLike', (req, res) => {
    //   console.log('Comment liked')
    //   db.collection('comments').updateOne({comments:req.body.comment,likes: req.body.likesS}, {
    //     $set: {
    //       likes:req.body.tLikes + 1
    //     }
    //   }, {
    //     sort: {_id: -1},
    //     upsert: true
    //   }, (err, result) => {
    //     if (err) return res.send(err)
    //     res.send(result)
    //   })
    // })

    // delete comment 
    app.delete('/deleteComment', (req, res) => {
      db.collection('comments').deleteOne({ comments: req.body.comment})
      .then(result => {
          console.log('Comment Deleted')
          res.json('Comment Deleted')
      })
      .catch(error => console.error(error))
  
  })
  app.listen(process.env.PORT || PORT, () => {
    console.log(`You're on ${PORT} babyeee`);
  });

  })

// update variable
const update = document.getElementsByClassName('fa-thumbs-up');
//delete variable
const remove = document.getElementsByClassName('fa-trash-alt');

//event listener for update 
// Array.from(update).forEach((element) => {
//   element.addEventListener('click',addLike)
// })

// async function addLike(){
//   console.log('clicked')
//   const comments = this.parentNode.parentNode.childNodes[1].innerText
//   const tLikes = Number(this.parentNode.parentNode.childNodes[3].innerText)
//   try{
//       const response = await fetch('addLike', {
//           method: 'put',
//           headers: {'Content-Type': 'application/json'},
//           body: JSON.stringify({
//             'comment': comments,
//             'likesS': tLikes
//           })
//         })
//       const data = await response.json()
//       console.log(data)
//       location.reload()
//   }catch(err){
//       console.log(err)
//   }
// }


//event listener for delete
Array.from(remove).forEach((element)=>{
  element.addEventListener('click', deleteComment)
})
//delete comment 
async function deleteComment(){
  const comments = this.parentNode.parentNode.childNodes[1].innerText
  console.log(comments)
  try{
      const response = await fetch('deleteComment', {
          method: 'delete',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify({
            'comment': comments
          })
        })
      const data = await response.json()
      console.log(data)
      location.reload()
      }catch (err) {
    console.log(err);
  }
}